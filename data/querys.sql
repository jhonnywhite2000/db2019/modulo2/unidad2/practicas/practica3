﻿SET NAMES 'utf8';

DROP DATABASE IF EXISTS dbp3;
CREATE DATABASE IF NOT EXISTS dbp3
CHARACTER SET utf8
COLLATE utf8_spanish_ci;

--
-- Set default database
--
USE dbp3;

CREATE OR REPLACE TABLE  delegacion(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(31),
  direccion varchar(63)
 );
CREATE OR REPLACE TABLE  trabajadores(
  id int AUTO_INCREMENT PRIMARY KEY,
  nombre varchar(31),
  apellidos varchar(31),
  fecha date,
  foto varchar(31),
  delegacion int,
  FOREIGN KEY(delegacion) REFERENCES delegacion(id)
);