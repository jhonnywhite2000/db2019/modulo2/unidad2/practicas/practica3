<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "trabajadores".
 *
 * @property int $id
 * @property string $nombre
 * @property string $apellidos
 * @property string $fecha
 * @property string $foto
 * @property int $delegacion
 *
 * @property Delegacion $delegacion0
 */
class Trabajadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'trabajadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'safe'],
            [['delegacion'], 'integer'],
            [['nombre', 'apellidos', 'foto'], 'string', 'max' => 31],
            [['delegacion'], 'exist', 'skipOnError' => true, 'targetClass' => Delegacion::className(), 'targetAttribute' => ['delegacion' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'fecha' => 'Fecha',
            'foto' => 'Foto',
            'delegacion' => 'Delegacion',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDelegacion0()
    {
        return $this->hasOne(Delegacion::className(), ['id' => 'delegacion']);
    }
}
