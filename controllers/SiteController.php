<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Delegacion;
use app\models\Trabajadores;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function  actionConsulta1(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Trabajadores::find()->distinct()->select("nombre, apellidos, fecha"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        return $this->render("listado", [
            "listados"=>$dataProvider,
            "campos"=>['nombre', 'apellidos', 'fecha'], 
            "enunciado"=>"Listar los empleados",
             "titulo"=>"Consulta1",
            "sql"=>"Select nombre, fecha apellidos from trabjadores",
        ]);
    }
    
    public function actionPrueba(){
        var_dump(\app\models\Trabajadores::find()->all());
        //phpinfo();
    }
    
    public function actionConsulta13(){
       $dataProvider = new ActiveDataProvider([
            'query' => Trabajadores::find()->distinct()->all(),
            ]); 
       
       return $this->render("vardunp", [
           "listados"=>$dataProvider
        ]);
    
       
    }
    
    
    public function actionConsulta15(){
       
        return $this->render("vardunp", [
        "listados" =>Delegacion::find()->all(),
        ]);
    }
    public function  actionConsulta14(){
        // mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' =>Delegacion::find()->distinct()->select("nombre, direccion"),
            'pagination'=>[
                'pageSize'=>10,
            ]
        ]);
        return $this->render("listado", [
            "listados"=>$dataProvider,
            "campos"=>['nombre', 'direccion'], 
            "enunciado"=>"Delegaciones",
             "titulo"=>"Consulta14",
            "sql"=>"Select * from delegacion",
        ]);
    }
    public function  actionConsulta14b(){
        // mediante DAO
      $numero=Yii::$app->
              db->
              createCommand("SELECT count(*) FROM delegacion")
              ->queryScalar();
              
        $dataProvider = new SqlDataProvider([
            'sql'=>'SELECT nombre, direccion from delegacion',
            //'totalCount'=>$numero,
            ]);
        return $this->render("listado", [
            "listados"=>$dataProvider,
            "campos"=>['nombre', 'direccion'], 
            "enunciado"=>"Delegaciones",
             "titulo"=>"Consulta 14",
            "sql"=>"SELECT * FROM delegacion ",
       ]);
    }
    

}
